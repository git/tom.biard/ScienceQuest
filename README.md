# Science Quest

## Accés rapide

- ### [Android](https://codefirst.iut.uca.fr/git/tom.biard/ScienceQuest/src/branch/Android/android)

- ### [API](https://codefirst.iut.uca.fr/git/tom.biard/ScienceQuest/src/branch/Springboot/SpringBootProject/)

- ### [Site Internet](https://codefirst.iut.uca.fr/git/tom.biard/ScienceQuest/src/branch/front/science-quest)

- ### [Documents](https://codefirst.iut.uca.fr/git/tom.biard/ScienceQuest/src/branch/master/Documentation)

## Description

Dans le cadre de l’apprentissage au sein du BUT Informatique de Clermont-Ferrand, un projet est établi tout au long de l’année afin de mettre en place une simulation d’entreprise gérée par les étudiants. Il répond à un besoin établi par un client, un professeur de l’IUT, ici **Mme Anaïs Durand** dans le rôle du Product Owner,  tout cela encadré par notre tutrice de projet **Mme Audrey Pouclet**.

Le projet repose sur la création d’un **site internet ayant pour but premier de faire découvrir des personnalités scientifiques féminines peu connues à travers des activités vidéoludiques** sous forme de mini-jeux, seul où à plusieurs, destinées aux enfants.

Les mini-jeux retenus sont les suivants:

- **Quizz (style Kahoot)** Des questions sont posées aux participants et ceux-ci doivent choisir une des 4 réponses proposées dans le temps imparti. Si la réponse est bonne le joueur gagne des points, le joueur ayant le plus de points gagne la partie !

- **Pendu (jeu solo)**
Le système tire un scientifique au hasard selon les paramètres de la partie.
Le joueur voit alors une photo du scientifique en plus de plusieurs indices (tel que sa nationalité ou encore sa date de naissance).
A partir de ces informations le joueur devra retrouver le nom du scientifique sous la forme d’un pendu
